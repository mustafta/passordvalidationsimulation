/*
 * Taghreed Safaryan
 *  991494905
 */
package passwordvalidationsimulation;
public class PasswordVlidator {
    
    private String password;
    //private char []array = password.toCharArray();
    private boolean isVlaid = false;
    private int digit=0;
    private int upperCase=0;

    public PasswordVlidator() {
    }

    public String getPassword() {
        return password;
    }

   
    public boolean isIsVlaid() {
        return isVlaid;
    }

    public int getDigit() {
        return digit;
    }

    public int getUpperCase() {
        return upperCase;
    }
    
      
    
    public boolean validPassword(String password) { 
      char []array = password.toCharArray();
      for (int i = 0; i<array.length; i++){
      if (Character.isDigit(array[i]))
              digit++;
      if (Character.isUpperCase(array[i]))
              upperCase++;    
      if(password.length() >= 8 && (digit!=0) && (upperCase!=0))
       isVlaid= true;
      }
   return isVlaid;
   }
}
    